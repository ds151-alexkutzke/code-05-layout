import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from './src/screens/HomeScreen';
import TopicsScreen from './src/screens/TopicsScreen';
import CounterScreen from './src/screens/CounterScreen';
import ColorListScreen from './src/screens/ColorListScreen';
import ColorCounterScreen from './src/screens/ColorCounterScreen';
import TextScreen from './src/screens/TextScreen';

const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Topics" component={TopicsScreen} />
        <Stack.Screen name="Counter" component={CounterScreen} />
        <Stack.Screen name="ColorList" component={ColorListScreen} />
        <Stack.Screen name="ColorCounter" component={ColorCounterScreen} />
        <Stack.Screen name="Text" component={TextScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;

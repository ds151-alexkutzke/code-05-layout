import React from 'react';
import { View, Button, Text, StyleSheet } from 'react-native';

const GeneralCounter = ({ labelInc, labelDec, onInc, onDec, value }) => {

  return (
    <View>
      <Button
        title={labelInc}
        onPress={ onInc }
      />
      <Button
        title={labelDec}
        onPress={ onDec }
      />
      <Text>
        { value }
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
});

export default GeneralCounter;

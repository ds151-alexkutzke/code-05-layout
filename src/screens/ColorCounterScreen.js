import React, { useState } from 'react';
import { View, StyleSheet } from 'react-native';
import GeneralCounter from '../components/GeneralCounter';

const ColorCounterScreen = () => {

  const [color, setColor] = useState({red:0, green: 0, blue: 0});

  function validateColor(colorToValidate) {
    if(colorToValidate < 0)
      return(0);
    else if(colorToValidate > 255)
      return(255);
    else 
      return(colorToValidate);
  }

  function updateColor(colorToUpdate, x){
    let newColor = {red: color.red, green: color.green, blue: color.blue };

    if(colorToUpdate === 'red') newColor.red = validateColor(newColor.red + x);
    else if(colorToUpdate === 'green') newColor.green = validateColor(newColor.green + x);
    else if(colorToUpdate === 'blue') newColor.blue = validateColor(newColor.blue + x);

    setColor(newColor);
  }

  return (
    <View>
      <GeneralCounter 
        labelInc="Incrementa Vermelho"
        labelDec="Decrementa Vermelho"
        onInc={() => updateColor('red', 10)}
        onDec={() => updateColor('red', -10)}
        value={color.red}
      />
      <GeneralCounter 
        labelInc="Incrementa Verde"
        labelDec="Decrementa Verde"
        onInc={() => updateColor('green', 10)}
        onDec={() => updateColor('green', -10)}
        value={color.green}
      />
      <GeneralCounter 
        labelInc="Incrementa Azul"
        labelDec="Decrementa Azul"
        onInc={() => updateColor('blue', 10)}
        onDec={() => updateColor('blue', -10)}
        value={color.blue}
      />
      <View style={[styles.colorView, {backgroundColor: `rgb(${color.red},${color.green},${color.blue})`}]}>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  colorView: {
    width: 100,
    height: 100,
    alignSelf: 'center',
  }
});

export default ColorCounterScreen;

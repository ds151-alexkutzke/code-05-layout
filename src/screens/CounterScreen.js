import React from 'react';
import { View, StyleSheet } from 'react-native';
import Counter from '../components/Counter';

const CounterScreen = () => {
  return (
    <View>
      <Counter />
    </View>
  )
}

const styles = StyleSheet.create({

});

export default CounterScreen;

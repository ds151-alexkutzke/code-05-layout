import React from 'react';
import { View, StyleSheet, Button } from 'react-native';

function HomeScreen({navigation}) {
  return (
    <View>
      <Button 
        title='Vai para Topics' 
        onPress={() => navigation.navigate('Topics')}
      />
      <Button 
        title='Vai para Counter' 
        onPress={() => navigation.navigate('Counter')}
      />
      <Button 
        title='Vai para ColorList' 
        onPress={() => navigation.navigate('ColorList')}
      />
      <Button 
        title='Vai para ColorCounter' 
        onPress={() => navigation.navigate('ColorCounter')}
      />
      <Button 
        title='Vai para Text' 
        onPress={() => navigation.navigate('Text')}
      />
    </View>
  );
}

const styles = StyleSheet.create({
});

export default HomeScreen;

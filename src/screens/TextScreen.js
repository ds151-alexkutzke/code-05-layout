import React, {useState} from 'react';
import { TextInput, View, Text, StyleSheet } from 'react-native';

const TextScreen = () => {
  const [text, setText] = useState('');
  return (
    <View>
      <TextInput 
        style={styles.textInput}
        placeholder="Digite aqui"
        onChangeText={(t) => setText(t)}
        onEndEditing={() => console.log('terminou de editar')}
        autoCapitalize='none'
        autoCorrect={false}
        value={text}
      />
      <Text>{text}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  textInput: {
    borderWidth: 1,
    borderColor: 'black',
    margin: 10,
    padding: 5,
  }
});

export default TextScreen;

